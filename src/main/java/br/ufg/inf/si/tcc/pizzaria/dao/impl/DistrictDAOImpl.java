package br.ufg.inf.si.tcc.pizzaria.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.ufg.inf.si.tcc.pizzaria.dao.DistrictDAO;
import br.ufg.inf.si.tcc.pizzaria.dao.generics.DAOGenericImpl;
import br.ufg.inf.si.tcc.pizzaria.models.District;

@Repository
@Transactional
public class DistrictDAOImpl extends DAOGenericImpl<District> implements DistrictDAO {

	@Override
	public boolean isExists(District entity) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<District> query = criteriaBuilder.createQuery(District.class);
		Root<District> root = query.from(District.class);

		Path<String> namePath = root.<String> get("name");

		ArrayList<Predicate> predicates = new ArrayList<>();
		if (!entity.getName().isEmpty()) {
			predicates.add(criteriaBuilder.equal(namePath, entity.getName()));
		}
		query.where(predicates.toArray(new Predicate[0]));
		try {
			TypedQuery<District> queryResult = manager.createQuery(query);
			queryResult.getSingleResult();
			return true;
		} catch (NoResultException e) {
			return false;
		}
	}

	@Override
	public List<District> findByCity(String name) {
		CriteriaBuilder cb = manager.getCriteriaBuilder();
		CriteriaQuery<District> criteriaQuery = cb.createQuery(District.class);
		Root<District> districtRoot = criteriaQuery.from(District.class);
		Path<String> path = districtRoot.join("city").<String> get("name");
		
		districtRoot.fetch("city");
		
		CriteriaQuery<District> select = criteriaQuery.select(districtRoot);
		select.where(cb.equal(path, name));
		
		try {
			TypedQuery<District> queryResult = manager.createQuery(select);
			return queryResult.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}

}
