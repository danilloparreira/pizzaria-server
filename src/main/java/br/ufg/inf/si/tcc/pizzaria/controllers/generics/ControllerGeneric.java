package br.ufg.inf.si.tcc.pizzaria.controllers.generics;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import br.ufg.inf.si.tcc.pizzaria.dao.generics.DAOGeneric;
import br.ufg.inf.si.tcc.pizzaria.models.generics.AbstractEntity;

public abstract interface ControllerGeneric<T extends AbstractEntity, DAO extends DAOGeneric<T>> {

	public ResponseEntity<T> find(Long id);

	public ResponseEntity<List<T>> findAll();

	public ResponseEntity<Void> save(T entity, UriComponentsBuilder ucBuilder);

	public ResponseEntity<T> merge(Long id, T entity);

	public ResponseEntity<T> delete(Long id);

}
