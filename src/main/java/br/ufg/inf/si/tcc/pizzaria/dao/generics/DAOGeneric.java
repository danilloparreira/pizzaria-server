package br.ufg.inf.si.tcc.pizzaria.dao.generics;

import java.util.List;

import br.ufg.inf.si.tcc.pizzaria.models.generics.AbstractEntity;

public abstract interface DAOGeneric<T extends AbstractEntity> {

	public T find(Long id);

	public List<T> findAll();

	public T save(T entity);

	public T merge(T entity);

	public void delete(T entity);

	public abstract boolean isExists(T entity);
}
