package br.ufg.inf.si.tcc.pizzaria.dao;

import br.ufg.inf.si.tcc.pizzaria.dao.generics.DAOGeneric;
import br.ufg.inf.si.tcc.pizzaria.models.State;

public interface StateDAO extends DAOGeneric<State> {
	public State find(String uf);
}
