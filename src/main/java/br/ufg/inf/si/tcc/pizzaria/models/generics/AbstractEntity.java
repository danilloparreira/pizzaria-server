package br.ufg.inf.si.tcc.pizzaria.models.generics;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
public class AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	@Version
	@Column(columnDefinition = "integer DEFAULT 0", nullable = false)
	private Long version;
//	@Column(updatable=false)
//	private LocalDateTime dateRegister;
//	private LocalDateTime dateUpdate;
	
	/**
	 * Construtor da classe AbstractEntity, que pega a data atual e referencia
	 * na data de cadastro e data de atualização
	 */
	public AbstractEntity() {
//		this.dateRegister = LocalDateTime.now();
//		this.dateUpdate = LocalDateTime.now();
	}

	/**
	 * Retorna o id da entidade
	 * 
	 * @return o id da entidade
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Retorna a data e hora da ultima modificação
	 * 
	 * @return a data e hora da ultima modificação
	 */
//	public LocalDateTime getDateUpdate() {
//		return dateUpdate;
//	}
//	public Long getDateUpdate() {
//		return dateUpdate.toInstant(ZoneOffset.UTC).toEpochMilli();
//	}
//
//	/**
//	 * Atualização da data da ultima modificação
//	 */
//	public void setDateUpdate() {
//		this.dateUpdate = LocalDateTime.now();
//	}
//
//	/**
//	 * Retorna a data e hora de cadastro
//	 * 
//	 * @return a data e hora de cadastro
//	 */
//	
//	public long getDateRegister() {
//		return dateRegister.toInstant(ZoneOffset.UTC).toEpochMilli();
////		return dateRegister;
//	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

}
