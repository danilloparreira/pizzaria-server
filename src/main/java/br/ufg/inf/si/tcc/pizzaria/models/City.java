package br.ufg.inf.si.tcc.pizzaria.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import br.ufg.inf.si.tcc.pizzaria.models.generics.AbstractEntity;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class City extends AbstractEntity {

	private static final long serialVersionUID = 1L;
	@Column(unique = true)
	private String name;

	@XmlTransient
	@ManyToOne
	@JoinColumn(name = "STATE_ID", referencedColumnName = "ID")
	// @JsonManagedReference
	// @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
	// property = "id")
	private State state;
	//
	// @OneToMany(targetEntity = District.class, mappedBy = "city", fetch =
	// FetchType.LAZY)
	// @Fetch(FetchMode.SELECT)
	// @JsonBackReference
	// private List<District> districts;

	public City() {
		super();
	}

	public City(String name, State state) {
		super();
		this.name = name;
		setState(state);
		// this.districts = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
		// this.state.addCity(this);
	}

	@Override
	public String toString() {
		return "name\t" + name + "\nstate\n" + state;
	}

	// public List<District> getDistricts() {
	// return districts;
	// }
	//
	// public void setDistricts(List<District> districts) {
	// this.districts = districts;
	// }

}
