package br.ufg.inf.si.tcc.pizzaria.controllers;

import br.ufg.inf.si.tcc.pizzaria.controllers.generics.ControllerGeneric;
import br.ufg.inf.si.tcc.pizzaria.dao.StateDAO;
import br.ufg.inf.si.tcc.pizzaria.models.State;

public interface StateController extends ControllerGeneric<State, StateDAO> {

}
