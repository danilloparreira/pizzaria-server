package br.ufg.inf.si.tcc.pizzaria.dao.impl;

import java.util.ArrayList;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.ufg.inf.si.tcc.pizzaria.dao.StateDAO;
import br.ufg.inf.si.tcc.pizzaria.dao.generics.DAOGenericImpl;
import br.ufg.inf.si.tcc.pizzaria.models.State;

@Repository
@Transactional
public class StateDAOImpl extends DAOGenericImpl<State> implements StateDAO {

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public boolean isExists(State state) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<State> query = criteriaBuilder.createQuery(State.class);
		Root<State> root = query.from(State.class);

		Path<String> namePath = root.<String> get("name");

		ArrayList<Predicate> predicates = new ArrayList<>();
		if (!state.getName().isEmpty()) {
			predicates.add(criteriaBuilder.equal(namePath, state.getName()));
		}
		query.where(predicates.toArray(new Predicate[0]));
		try {
			TypedQuery<State> queryResult = manager.createQuery(query);
			queryResult.getSingleResult();
			return true;
		} catch (NoResultException e) {
			return false;
		}
	}

	@Override
	public State find(String uf) {

		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<State> query = criteriaBuilder.createQuery(State.class);
		Root<State> root = query.from(State.class);

		Path<String> namePath = root.<String> get("uf");

		ArrayList<Predicate> predicates = new ArrayList<>();
		if (!uf.isEmpty()) {
			predicates.add(criteriaBuilder.equal(namePath, uf));
		}
		query.where(predicates.toArray(new Predicate[0]));
		try {
			TypedQuery<State> queryResult = manager.createQuery(query);
			return queryResult.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
