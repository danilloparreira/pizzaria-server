package br.ufg.inf.si.tcc.pizzaria.dao;

import java.util.List;

import br.ufg.inf.si.tcc.pizzaria.dao.generics.DAOGeneric;
import br.ufg.inf.si.tcc.pizzaria.models.District;

public interface DistrictDAO extends DAOGeneric<District> {
	public List<District> findByCity(String name);
}
