package br.ufg.inf.si.tcc.pizzaria.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import br.ufg.inf.si.tcc.pizzaria.models.State;

@EnableTransactionManagement
public class JPAConfig {

	private static final String[] MYSQL = {
			"com.mysql.jdbc.Driver",
			"jdbc:mysql://localhost/pizzaria?autoReconnect=true&useSSL=false",
			"root", "" };
	private static final String[] POSTGRES = {
			"org.postgresql.Driver",
			"jdbc:postgresql://localhost/pizzaria",
			"postgres", "" };

	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		String[] BancoDeDados = MYSQL;
		dataSource.setDriverClassName(BancoDeDados[0]);
		dataSource.setUrl(BancoDeDados[1]);
		dataSource.setUsername(BancoDeDados[2]);
		dataSource.setPassword(BancoDeDados[3]);

		return dataSource;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean getEntityManagerFactory(DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();

		entityManagerFactory.setPackagesToScan(packageName(State.class));
		entityManagerFactory.setDataSource(dataSource);

		entityManagerFactory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		entityManagerFactory.setJpaProperties(getProperties());

		return entityManagerFactory;
	}

	private Properties getProperties() {
		Properties props = new Properties();
		props.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
		props.setProperty("hibernate.show_sql", "true");
//		props.setProperty("hibernate.format_sql", "true");
		props.setProperty("hibernate.hbm2ddl.auto", "create-drop");
		// props.setProperty("hibernate.hbm2ddl.auto", "update");
		return props;
	}

	@Bean
	public JpaTransactionManager getTransactionManager(EntityManagerFactory emf) {
		return new JpaTransactionManager(emf);
	}

	private String packageName(Class<?> clazz) {
		return clazz.getPackage().getName();
	}
}