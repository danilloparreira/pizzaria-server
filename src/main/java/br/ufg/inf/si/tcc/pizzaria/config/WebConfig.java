package br.ufg.inf.si.tcc.pizzaria.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewInterceptor;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import br.ufg.inf.si.tcc.pizzaria.controllers.StateController;
import br.ufg.inf.si.tcc.pizzaria.dao.StateDAO;

@Configuration
@EnableWebMvc
@ComponentScan(basePackageClasses = { StateController.class, StateDAO.class })
@EnableTransactionManagement
public class WebConfig extends WebMvcConfigurerAdapter {

}
