package br.ufg.inf.si.tcc.pizzaria.controllers.impl;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufg.inf.si.tcc.pizzaria.controllers.StateController;
import br.ufg.inf.si.tcc.pizzaria.controllers.generics.ControllerGenericImpl;
import br.ufg.inf.si.tcc.pizzaria.dao.StateDAO;
import br.ufg.inf.si.tcc.pizzaria.models.State;

@RestController
@RequestMapping("/state")
public class StateControllerImpl extends ControllerGenericImpl<State, StateDAO> implements StateController {
}
