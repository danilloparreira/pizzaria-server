package br.ufg.inf.si.tcc.pizzaria.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.ufg.inf.si.tcc.pizzaria.models.generics.AbstractEntity;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class District extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@Column(unique = true)
	private String name;

	private Double deliveryFee;

	@XmlTransient
	@ManyToOne
	@JoinColumn(name = "CITY_ID", referencedColumnName = "ID")
	@JsonManagedReference
	private City city;

	public District() {
		super();
	}

	public District(String name, Double deliveryFee, City city) {
		super();
		this.name = name;
		this.deliveryFee = deliveryFee;
		this.city = city;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getDeliveryFee() {
		return deliveryFee;
	}

	public void setDeliveryFee(Double deliveryFee) {
		this.deliveryFee = deliveryFee;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "\nname:\t" + name + "\ndeliveryFee:\t" + deliveryFee + "\ncity:\t" + city;
	}
}
