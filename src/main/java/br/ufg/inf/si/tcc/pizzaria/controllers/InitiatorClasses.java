package br.ufg.inf.si.tcc.pizzaria.controllers;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufg.inf.si.tcc.pizzaria.dao.CityDAO;
import br.ufg.inf.si.tcc.pizzaria.dao.DistrictDAO;
import br.ufg.inf.si.tcc.pizzaria.dao.StateDAO;
import br.ufg.inf.si.tcc.pizzaria.models.City;
import br.ufg.inf.si.tcc.pizzaria.models.District;
import br.ufg.inf.si.tcc.pizzaria.models.State;

@Component
public class InitiatorClasses {
	//
	@Autowired(required=true)
	private StateDAO stateDAO;
	@Autowired(required=true)
	private CityDAO cityDAO;
	@Autowired(required=true)
	private DistrictDAO districtDAO;

//	@PostConstruct
	public void init() {
		stateDAO.save(new State("São Paulo", "SP"));
		stateDAO.save(new State("Tocantins", "TO"));
		stateDAO.save(new State("Rio de Janeiro", "RJ"));
		State stateSave = stateDAO.save(new State("Goiás", "GO"));
		City city = cityDAO.save(new City("Goiânia", stateSave));
		City city2 = cityDAO.save(new City("Trindade", stateSave));
		City city3 = cityDAO.save(new City("Anapolis", stateSave));
		City city4 = cityDAO.save(new City("Goiânira", stateSave));
		districtDAO.save(new District("Jardim Nova Esperança", 3.0, city));
		districtDAO.save(new District("Cândida de Moraes", 3.0, city));
		districtDAO.save(new District("Santos Dummont Inicio", 4.0, city));
		districtDAO.save(new District("Santos Dummont Final", 5.0, city));
		districtDAO.save(new District("Ed. Tropicale", 4.0, city));
		districtDAO.save(new District("Ed. Terra Mundi", 6.0, city));
		districtDAO.save(new District("Cond. Malibu I e II", 5.0, city));
		districtDAO.save(new District("Cond. Malibu III", 5.0, city));
		districtDAO.save(new District("Jardim Curitiba IV", 7.0, city));
		districtDAO.save(new District("Jardim Curitiba III", 7.0, city));
		districtDAO.save(new District("Jardim Curitiba II", 8.0, city));
		districtDAO.save(new District("Jardim Curitiba I", 7.0, city));
		System.out.println(stateDAO.find("GO").toString());
		districtDAO.findByCity("Goiânia").forEach(System.out::println);
	}
}
