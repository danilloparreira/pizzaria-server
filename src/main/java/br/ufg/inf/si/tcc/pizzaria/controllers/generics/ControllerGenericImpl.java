package br.ufg.inf.si.tcc.pizzaria.controllers.generics;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import br.ufg.inf.si.tcc.pizzaria.dao.generics.DAOGeneric;
import br.ufg.inf.si.tcc.pizzaria.models.generics.AbstractEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin//(origins = "http://localhost:3000", maxAge = 3600)
public abstract class ControllerGenericImpl<T extends AbstractEntity, DAO extends DAOGeneric<T>>
		implements ControllerGeneric<T, DAO> {

	@Autowired
	private DAO dao;

	@SuppressWarnings({ "rawtypes" })
	private String getClazz() {
		Class clazz = (Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		return clazz.getSimpleName();
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<T> find(@PathVariable("id") Long id) {
		System.out.println("Find " + getClazz() + " with id: " + id);
		T entity = (T) dao.find(id);
		if (entity == null) {
			System.out.println(getClazz() + " with id " + id + " not found.");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(entity, HttpStatus.OK);
	}

	@Override
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<T>> findAll() {
		List<T> entity = dao.findAll();
		if (entity.isEmpty()) {
			return new ResponseEntity<List<T>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<T>>(entity, HttpStatus.OK);

	}

	@Override
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Void> save(@RequestBody T entity, UriComponentsBuilder ucBuilder) {
		System.out.println("Creating " + getClazz() + " " + entity);
		
		if (dao.isExists(entity)) {
			System.out.println("The " + getClazz() + " " + entity + " is already registered.");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		System.out.println("Tentando salvar");
		System.out.println(entity);
		dao.save(entity);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(
				ucBuilder.path("/" + getClazz().toLowerCase() + "/{id}").buildAndExpand(entity.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<T> merge(@PathVariable("id") Long id, @RequestBody T entity) {
		System.out.println("Updating " + getClazz() + " " + id);

		T currentEntity = dao.find(id);

		System.out.println(currentEntity);
		if (currentEntity == null) {
			System.out.println(getClazz() + " with id " + id + " was not found.");
			return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
		}
		if (currentEntity.equals(entity)) {
			System.out.println("As entidades são iguais.");
		}

		try {
			T entityUpdate = dao.merge(entity);
			return new ResponseEntity<T>(entityUpdate, HttpStatus.OK);
		} catch (DataIntegrityViolationException e) {
			return new ResponseEntity<T>(HttpStatus.CONFLICT);
		}
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<T> delete(@PathVariable("id") Long id) {
		System.out.println("Fetching & Deleting " + getClazz() + " with id " + id);

		T entity = dao.find(id);
		if (entity == null) {
			System.out.println("Could not delete. The " + getClazz() + " with the id" + id + " was not found.");
			return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
		}
		try {
			dao.delete(entity);
			return new ResponseEntity<T>(HttpStatus.NO_CONTENT);
		} catch (DataIntegrityViolationException e) {
			System.out.println("Unable to remove the " + getClazz() + ", because there is a dependency");
			return new ResponseEntity<T>(HttpStatus.CONFLICT);
		}
	}
}
