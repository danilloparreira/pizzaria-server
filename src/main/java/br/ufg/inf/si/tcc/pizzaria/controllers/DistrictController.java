package br.ufg.inf.si.tcc.pizzaria.controllers;

import br.ufg.inf.si.tcc.pizzaria.controllers.generics.ControllerGeneric;
import br.ufg.inf.si.tcc.pizzaria.dao.DistrictDAO;
import br.ufg.inf.si.tcc.pizzaria.models.District;

public interface DistrictController extends ControllerGeneric<District, DistrictDAO> {

}
