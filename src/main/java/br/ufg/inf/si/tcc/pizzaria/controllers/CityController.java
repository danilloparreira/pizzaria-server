package br.ufg.inf.si.tcc.pizzaria.controllers;

import br.ufg.inf.si.tcc.pizzaria.controllers.generics.ControllerGeneric;
import br.ufg.inf.si.tcc.pizzaria.dao.CityDAO;
import br.ufg.inf.si.tcc.pizzaria.models.City;

public interface CityController extends ControllerGeneric<City, CityDAO> {

}
