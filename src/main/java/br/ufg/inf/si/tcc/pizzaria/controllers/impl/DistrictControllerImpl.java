package br.ufg.inf.si.tcc.pizzaria.controllers.impl;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufg.inf.si.tcc.pizzaria.controllers.DistrictController;
import br.ufg.inf.si.tcc.pizzaria.controllers.generics.ControllerGenericImpl;
import br.ufg.inf.si.tcc.pizzaria.dao.DistrictDAO;
import br.ufg.inf.si.tcc.pizzaria.models.District;

@RestController
@RequestMapping("/district")
public class DistrictControllerImpl extends ControllerGenericImpl<District, DistrictDAO> implements DistrictController {

}
