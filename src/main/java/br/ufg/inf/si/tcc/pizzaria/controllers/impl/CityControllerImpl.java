package br.ufg.inf.si.tcc.pizzaria.controllers.impl;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufg.inf.si.tcc.pizzaria.controllers.CityController;
import br.ufg.inf.si.tcc.pizzaria.controllers.generics.ControllerGenericImpl;
import br.ufg.inf.si.tcc.pizzaria.dao.CityDAO;
import br.ufg.inf.si.tcc.pizzaria.models.City;

@RestController
@RequestMapping("/city")
public class CityControllerImpl extends ControllerGenericImpl<City, CityDAO> implements CityController {

}
