package br.ufg.inf.si.tcc.pizzaria.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import br.ufg.inf.si.tcc.pizzaria.models.generics.AbstractEntity;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class State extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@Column(unique = true)
	private String name;
	@Column(unique = true, length = 2)
	private String uf;
	//
	// @OneToMany(targetEntity = City.class, mappedBy = "state", fetch =
	// FetchType.LAZY)
	// @Fetch(FetchMode.SELECT)
	// @JsonBackReference
	// private List<City> cities;

	public State() {
		super();
	}

	public State(String name, String uf) {
		super();
		// cities = new ArrayList<>();
		this.name = name;
		this.setUf(uf);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf.toUpperCase();
	}

	// public List<City> getCities() {
	// return cities;
	// }
	//
	// public void setCities(List<City> cities) {
	// this.cities = cities;
	// }
	//
	// public void addCity(City... cities) {
	// for (City city : cities) {
	// this.cities.add(city);
	// }
	// }

	@Override
	public String toString() {
		return "id\t" + id + "\nname\t" + name + "\nuf\t" + uf;
	}

}
