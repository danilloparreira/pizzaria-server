package br.ufg.inf.si.tcc.pizzaria.dao.impl;

import java.util.ArrayList;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.ufg.inf.si.tcc.pizzaria.dao.CityDAO;
import br.ufg.inf.si.tcc.pizzaria.dao.generics.DAOGenericImpl;
import br.ufg.inf.si.tcc.pizzaria.models.City;

@Repository
@Transactional
public class CityDAOImpl extends DAOGenericImpl<City> implements CityDAO {

	@Override
	public boolean isExists(City city) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<City> query = criteriaBuilder.createQuery(City.class);
		Root<City> root = query.from(City.class);

		Path<String> namePath = root.<String> get("name");

		ArrayList<Predicate> predicates = new ArrayList<>();
		if (!city.getName().isEmpty()) {
			predicates.add(criteriaBuilder.equal(namePath, city.getName()));
		}
		query.where(predicates.toArray(new Predicate[0]));
		try {
			TypedQuery<City> queryResult = manager.createQuery(query);
			queryResult.getSingleResult();
			return true;
		} catch (NoResultException e) {
			return false;
		}
	}

	@Override
	public City find(String name) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<City> query = criteriaBuilder.createQuery(City.class);
		Root<City> root = query.from(City.class);

		Path<String> namePath = root.<String> get("name");
		ArrayList<Predicate> predicates = new ArrayList<>();
		if (!name.isEmpty()) {
			predicates.add(criteriaBuilder.equal(namePath, name));
		}
		query.where(predicates.toArray(new Predicate[0]));
		try {
			TypedQuery<City> queryResult = manager.createQuery(query);
			return queryResult.getSingleResult();
		} catch (NoResultException e) {
			return new City();
		} 
	}

}
