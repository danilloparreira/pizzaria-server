package br.ufg.inf.si.tcc.pizzaria.dao;

import br.ufg.inf.si.tcc.pizzaria.dao.generics.DAOGeneric;
import br.ufg.inf.si.tcc.pizzaria.models.City;

public interface CityDAO extends DAOGeneric<City> {
	public City find(String name);
}
