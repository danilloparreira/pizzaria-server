package br.ufg.inf.si.tcc.pizzaria.dao.generics;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.ufg.inf.si.tcc.pizzaria.models.generics.AbstractEntity;

@Transactional
@SuppressWarnings("unchecked")
public abstract class DAOGenericImpl<T extends AbstractEntity> implements DAOGeneric<T> {

	private Class<? extends T> clazz;
	@PersistenceContext
	protected EntityManager manager;

	public DAOGenericImpl() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.clazz = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public T find(Long id) {
		return manager.find(clazz, id);
	}

	@Override
	public List<T> findAll() {
		List<T> resultList = (List<T>) manager.createQuery("Select t from " + clazz.getName() + " t").getResultList();
		return resultList;

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public T save(T entity) {
		manager.persist(entity);
		return entity;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public T merge(T entity) {
//		entity.setDateUpdate();
		return manager.merge(entity);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(T entity) {
		entity = this.manager.merge(entity);
		this.manager.remove(entity);

	}

}
